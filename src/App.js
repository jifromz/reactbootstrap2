import React from "react";
import "./App.css";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Home from "./components/Home";
import About from "./components/About";
import News from "./components/News";
import Navbar from './components/CustomNavbar';

function App() {
  return (
    <Router>
      <div>
      <Navbar />
        <Route exact path="/" component={Home}></Route>
        <Route path="/about" component={About}></Route>
        <Route path="/news" component={News}></Route>
      </div>
    </Router>
  );
}
 
export default App;
