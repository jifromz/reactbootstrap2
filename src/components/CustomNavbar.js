import React, { Component } from "react";
import { Navbar, Nav, NavItem, NavLink } from "react-bootstrap";
import { Link } from "react-router-dom";
import "./CustomNavbar.css";

export default class CustomNavbar extends Component {
  render() {
    return (
      <Navbar bg="dark" variant="dark">
        <Navbar.Brand>
          <Link className="nav-link" to="/home">
            Codelife
          </Link>
        </Navbar.Brand>
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto" navbar>
            <NavItem>
              <Nav.Link tag={Link} href="/">
                Home
              </Nav.Link>
            </NavItem>
            <NavItem>
              <Nav.Link tag={Link} href="/about">
                About
              </Nav.Link>
            </NavItem>
            <NavItem>
              <Nav.Link tag={Link} href="/news">
                News
              </Nav.Link>
            </NavItem>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}
