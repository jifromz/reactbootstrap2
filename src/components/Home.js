import React from "react";
import { Link } from "react-router-dom";
import { Jumbotron, Container, Row, Col, Image, Button } from "react-bootstrap";

export default class Home extends React.Component {
  render() {
    return (
      <Container>
        <Jumbotron>
          <h2>Welcome to codelif.ui</h2>
          <p>This is how to build a we bsite using react bootstrap</p>
        </Jumbotron>
        <Link to="/about">
          <Button variant="primary">About</Button>
        </Link>
      </Container>
    );
  }
}
